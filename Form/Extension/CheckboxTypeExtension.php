<?php

namespace Iweigel\AceTemplateBundle\Form\Extension;

use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;

/**
 * Extension for Checkbox type.
 */
class CheckboxTypeExtension extends AbstractTypeExtension
{
    /**
     * {@inheritdoc}
     */
    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        if (isset($options['ace'])) {
            $view->vars['ace'] = $options['ace'];
        } else {
            $view->vars['ace'] = false;
        }
        if (isset($options['ace_label'])) {
            $view->vars['ace_label'] = $options['ace_label'];
        } else {
            $view->vars['ace_label'] = false;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setOptional(array(
            'ace',
            'ace_label',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getExtendedType()
    {
        return 'checkbox';
    }
}
